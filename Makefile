DOCUMENTS = thesis.pdf

TEXINPUTS = texmf//:

DEPS_DIR = .deps
LATEXMK = ./latexmk -recorder -bibtex -use-make -deps \
	-pdflatex="pdflatex -interaction=nonstopmode -halt-on-error" \
	-latex="latex -interaction=nonstopmode -halt-on-error" \
	-e 'warn qq(In Makefile, turn off custom dependencies);' \
	-e '@cus_dep_list = ();' \
	-e 'show_cus_dep();'
DEPS = $(wildcard $(DEPS_DIR)/*)

# dubiously speed up by eliminating all implicit rules
.SUFFIXES: 

.DEFAULT_GOAL := all

#make TEXINPUTS paths available for child processes
export TEXINPUTS

# Include all dependencies found in the dependency dir
include $(DEPS)

.PHONY: all
all: $(DOCUMENTS)

%.pdf: %.tex $(DEPS_DIR)
	$(LATEXMK) -pdf -M -MP -MF $(DEPS_DIR)/$@P $<

%.dvi: %.tex $(DEPS_DIR)
	$(LATEXMK) -dvi -M -MP -MF $(DEPS_DIR)/$@P $<

%.ps: %.tex $(DEPS_DIR)
	$(LATEXMK) -ps -M -MP -MF $(DEPS_DIR)/$@P $<

.PHONY: clean
clean:
	-$(LATEXMK) -C -deps- -quiet
	-rm -rf .deps *.run.xml

$(DEPS_DIR):
	mkdir $@

# auxiliary build rules for document dependencies
%.pdf: %.svg
	inkscape --export-pdf=$@ --export-dpi=600 $<

%.pdf_tex: %.svg
	inkscape --export-pdf=$(subst _tex,,$@) --export-latex --export-dpi=600 $<

%.png: %.svg
	inkscape --export-png=$@ --export-dpi=600 $<
